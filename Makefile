DESTDIR ?=

# /nix is the mount point of the /nix directory present in nix application snaps
install::
	install -m 755 -d $(DESTDIR)/nix

# /dev, /sys and /proc are mount points for the usual suspects.
install::
	install -m 755 -d $(DESTDIR)/dev
	install -m 755 -d $(DESTDIR)/proc
	install -m 755 -d $(DESTDIR)/sys

# /tmp is a mount point for private per-snap view of tmp
install::
	install -m 755 -d $(DESTDIR)/tmp

# /run provides the view of host's /run directory
install::
	install -m 755 -d $(DESTDIR)/run

# /home and /root provide the view of the corresponding directories on the host.
install::
	install -m 755 -d $(DESTDIR)/home
	install -m 755 -d $(DESTDIR)/root

# /etc provides a view of host's /etc
install::
	install -m 755 -d $(DESTDIR)/etc

# /media provides the view of host's preferred media mount point
install::
	install -m 755 -d $(DESTDIR)/media

# /snap provides the
install::
	install -m 755 -d $(DESTDIR)/snap

# /bin/snapctl is a symbolic link to snapctl injected by snapd into the base snap,
# at runtime, with allows snap application processes to interact with snapd over
# an internal socket.
install::
	install -m 755 -d $(DESTDIR)/bin
	ln -fs ../usr/lib/snapd/snapctl $(DESTDIR)/bin/snapctl

# /usr/lib/snapd is a mount point for internal snapd binaries
# This is also where we get snapctl from
install::
	install -m 755 -d $(DESTDIR)/usr
	install -m 755 -d $(DESTDIR)/usr/lib
	install -m 755 -d $(DESTDIR)/usr/lib/snapd

# /var/lib/snapd is a mount point for internal snapd state files
install::
	install -m 755 -d $(DESTDIR)/var
	install -m 755 -d $(DESTDIR)/var/lib
	install -m 755 -d $(DESTDIR)/var/lib/snapd

# /var/snap is where system wide, per-snap data is stored
install::
	install -m 755 -d $(DESTDIR)/var
	install -m 755 -d $(DESTDIR)/var/snap

# /var/tmp provides the view of the host's /var/run directory.
install::
	install -m 755 -d $(DESTDIR)/var
	install -m 755 -d $(DESTDIR)/var/tmp

# /var/log provides the view of the host's /var/log directory.
install::
	install -m 755 -d $(DESTDIR)/var
	install -m 755 -d $(DESTDIR)/var/log

# /usr/src provides the view of the host's /usr/src directory.
install::
	install -m 755 -d $(DESTDIR)/usr
	install -m 755 -d $(DESTDIR)/usr/src
